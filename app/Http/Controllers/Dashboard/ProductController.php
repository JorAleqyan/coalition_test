<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\ProductRequest;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = json_decode(file_get_contents('../database/products.json'));

        if(!$products){
            $products = [];
        }


        return view('dashboard.products.index', compact('products'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  ProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
       // Product::create($request->all());
        $data = $request->all();
        unset($data['_token']);
        $data['total'] =  $data['quantity'] *  $data['price'];
        $db = file_get_contents('../database/products.json');
        $db = json_decode($db, true);
        if(!$db){
            $db = [];
        }
        $db[] = $data;
        $db = json_encode($db,  JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
        file_put_contents('../database/products.json', $db);
        $products = json_decode(file_get_contents('../database/products.json'));
        return view('dashboard.products.list', compact('products'));
    }


}
