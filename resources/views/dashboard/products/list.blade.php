@php
$total = 0;
@endphp
<table class="table">
    <tr>
        <th>Name</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Total</th>
    </tr>
    @foreach($products as $product)
        <tr>
            <td>{{$product->name}}</td>
            <td>{{$product->quantity}}</td>
            <td>{{$product->price}}</td>
            <td>{{$product->total}}</td>
        </tr>
        @php
        $total += $product->total;
        @endphp
    @endforeach
    <tr>
        <td>-</td>
        <td>-</td>
        <td>-</td>
        <td>{{$total}}</td>
    </tr>
</table>