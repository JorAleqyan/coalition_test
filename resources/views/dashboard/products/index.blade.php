@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h5>Add Product</h5>
                </div>
                <div class="card-body">
                    <form id=product-form">
                        {{csrf_field()}}
                        <div class="form-group row"><label class="col-lg-2 col-form-label">Name</label>
                            <div class="col-lg-10">
                                <input type="text" placeholder="Name" class="form-control" name="name">
                            </div>
                        </div>
                        <div class="form-group row"><label class="col-lg-2 col-form-label">Quantity</label>
                            <div class="col-lg-10">
                                <input type="number" placeholder="Quantity" class="form-control" name="quantity" step="1">
                            </div>
                        </div>
                        <div class="form-group row"><label class="col-lg-2 col-form-label">Price</label>
                            <div class="col-lg-10">
                                <input type="number" placeholder="Price" class="form-control" name="price">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-sm btn-success" type="button" id="submit-button">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Products</h5>
                        </div>
                        <div class="card-body" id="products-table">
                          @include('dashboard.products.list')
                        </div>
                    </div>
                </div>
    </div>
    </div>

    <script>
        $( document ).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#submit-button').click(function(){
                // var formData = $("#product-form").serialize();
                var formData = {
                    name: $( "input[name='name']").val(),
                    quantity: $( "input[name='quantity']" ).val(),
                    price: $( "input[name='price']" ).val(),
                    _token: $('meta[name="csrf-token"]').attr('content')
                };
                $.ajax({
                    url: '/dashboard/products',
                    type: "POST",
                    data: formData,
                    success: function (data) {
                        $('#products-table').html('');
                       $('#products-table').append(data);
                        $( "input[name='name']").val('');
                        $( "input[name='quantity']" ).val('');
                        $( "input[name='price']" ).val('');
                    }
                });
            })


        });
    </script>

@endsection